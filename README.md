sheep for .NET Core(C#)

# Requirements

 - [DotNetCore] 1.0

# Development

Prepare:

    % dotnet restore

Run:

    % dotnet run

# License

[WTFPL]

[DotNetCore]: https://dotnet.github.io
[WTFPL]: http://www.wtfpl.net "WTFPL"
