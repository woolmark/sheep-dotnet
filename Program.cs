﻿namespace Woolmark.Sheep
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Collections.Generic;

    public class Program
    {
        public static void Main(string[] args)
        {
            var prog = new Program();
            prog.RunAsync().Wait();
        }

        async Task<int> RunAsync()
        {
            var frame = new Frame();
            var flock = new List<Sheep> { new Sheep(frame) };

            await Task.Run(() => {
                while(flock.Count > 0)
                {
                    var newFlock = new List<Sheep>();
                    foreach(var sheep in flock)
                    {
                        sheep.Move();
                        Console.WriteLine(sheep);

                        if (sheep.IsMaximalPosition())
                        {
                            Console.WriteLine("JUMP!!");
                        }
                        if (!sheep.IsGoAway())
                        {
                            newFlock.Add(sheep);
                        }
                    }

                    flock = newFlock;
                    Thread.Sleep(100);
                }
            });

            return 0;
        }
    }
}
