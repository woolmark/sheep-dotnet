﻿namespace Woolmark.Sheep
{
    using System;

    enum FrameSize
    {
        DefaultWidth = 120,
        DefaultHeight = 120
    }

    enum ImageSize
    {
        FenceWidth = 52,
        FenceHeight = 78,
        SheepWidth = 17,
        SheepHeight = 12
    }

    interface IRectangle
    {
        int X { get; }
        int Y { get; }
        int Width { get; }
        int Height { get; }
    }

    interface IScalable
    {
        int Scale{ get; }
    }

    interface IMovable
    {
        void Move();
    }

    struct Box : IRectangle
    {
        int IRectangle.X { get { return x; } }
        int IRectangle.Y { get { return y; } }
        int IRectangle.Width { get { return width; } }
        int IRectangle.Height { get { return height; } }

        int x;
        int y;
        int width;
        int height;
        public Box(int x, int y, int width, int height)
        {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
    }

    public class Frame : IRectangle, IScalable
    {
        int IScalable.Scale { get { return scale; } }

        int IRectangle.X { get { return 0; } }
        int IRectangle.Y { get { return 0; } }
        int IRectangle.Width { get { return width; } }
        int IRectangle.Height { get { return height; } }

        internal IRectangle Ground { get { return ground; } }
        internal IRectangle Fence { get { return fence; } }

        IRectangle ground;
        IRectangle fence;

        int width;
        int height;
        int scale;

        public Frame(int width = (int) FrameSize.DefaultWidth, int height = (int) FrameSize.DefaultHeight)
        {
            this.width = width;
            this.height = height;

            if (width < height)
            {
                scale = (int) (width / (int) FrameSize.DefaultWidth);
            }
            else
            {
                scale = (int) (height / (int) FrameSize.DefaultHeight);
            }

            if (scale < 1)
            {
                scale = 1;
            }

            var w = (int) ((int) ImageSize.FenceWidth * scale);
            var h = (int) ((int) ImageSize.FenceHeight * scale);
            fence = new Box((width  - w) / 2, height - h, w, h);

            h = (int) ((int) ImageSize.FenceHeight * scale * 0.9);
            ground = new Box(0, height - h, width, h);

        }

    }

    public class Sheep : IRectangle, IScalable, IMovable
    {

        enum MoveValue
        {
            X = 5,
            Y = 3
        }

        const int JumpFrame = 4;

        int IRectangle.X { get { return x; } }
        int IRectangle.Y { get { return y; } }
        int IRectangle.Width { get { return width; } }
        int IRectangle.Height { get { return height; } }
        int IScalable.Scale { get { return scale; } }

        int x;
        int y;
        int width;
        int height;
        int jumpState = -1;
        int jumpX;
        bool stretch;
        int scale;

        static Random random = new Random();

        public Sheep(Frame frame)
        {
            width = (int) ImageSize.SheepWidth * ((IScalable) frame).Scale;
            height = (int) ImageSize.SheepHeight * ((IScalable) frame).Scale;
            x = ((IRectangle) frame).Width;
            y = random.Next(frame.Ground.Y, ((IRectangle) frame).Height - height);

            jumpX = -1 * (y - ((IRectangle) frame).Height) * frame.Fence.Width / frame.Fence.Height + (((IRectangle) frame).Width - frame.Fence.Width) / 2;

            scale = ((IScalable) frame).Scale;
        }

        public void Move()
        {

            x -= (int) MoveValue.X * scale;

            if (jumpState < 0 && jumpX <= x && x < jumpX + (int) MoveValue.X * scale)
            {
                jumpState = 0;
            }

            if (jumpState >= 0)
            {
                if (jumpState < JumpFrame)
                {
                    y -= (int) MoveValue.Y * scale;
                }
                else if (jumpState < JumpFrame * 2)
                {
                    y += (int) MoveValue.Y * scale;
                }
                else
                {
                    jumpState = -2;
                }
                jumpState++;
                stretch = true;
            }
            else
            {
                stretch = !stretch;
            }

        }

        public bool IsGoAway()
        {
            return x < - (int) ImageSize.SheepHeight;
        }
        public bool IsMaximalPosition()
        {
            return jumpState == JumpFrame;
        }

        public override string ToString()
        {
            return "x:" + x + ", y:" + y + ", stretch:" + stretch;
        }
    }
}
